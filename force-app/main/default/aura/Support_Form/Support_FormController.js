({
    doInit: function(component, event, helper) {
        var action = component.get("c.getPiklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                component.set("v.BrandList", plValues);
            }
        });
       
       var action2 = component.get("c.getIssuePiklistValues");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                component.set("v.IssueList", plValues);
            }
        });
        $A.enqueueAction(action);
		$A.enqueueAction(action2);         
      
    },
    setCookies : function(cmp){
        var d = new Date();
        var exdays = 30;
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString(); 
        
        var supportemail = document.getElementById("email").value;
        var supportname = document.getElementById("name").value;
        var supportphone = document.getElementById("phone").value;
        var identifybyRadio = document.getElementsByName('00N2w000005hpKM');
        var identifybyValue;
        for(var i = 0; i < identifybyRadio.length; i++){
            if(identifybyRadio[i].checked){
                identifybyValue = identifybyRadio[i].value;
            }
        }
        var identifyby = identifybyValue;
        var supportsiteid = document.getElementById("00N2w000005hpKO").value;
        var supportstorenumber = document.getElementById("00N2w000005hpKe").value;
        var supportbrand = document.getElementById("00N2w000005hpKG").value;
        var sitephone = document.getElementById("00N2w000005hpKd").value;
        
        document.cookie = "supportemail=" + supportemail + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "supportname=" + supportname + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "supportphone=" + supportphone + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "identifyby=" + identifyby + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "supportsiteid=" + supportsiteid + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "supportstorenumber=" + supportstorenumber + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "supportbrand=" + supportbrand + ";" + expires + "; path=/; secure; samesite=strict; true;";
        document.cookie = "sitephone=" + sitephone + ";" + expires + "; path=/; secure; samesite=strict; true;";
    },
    deleteCookies: function(cmp){
        document.cookie = "supportemail= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "supportname= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "supportphone= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "identifyby= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "supportsiteid= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "supportstorenumber= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";  
        document.cookie = "supportbrand= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
        document.cookie = "sitephone= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true;";
    },
    render: function(cmp){
        document.getElementById('summary').style.display ='none';
        document.getElementById("00N2w000005hpKL").required = true;
        document.getElementById("00N2w000005hpKQ").required = true;
        document.getElementById('recordType').value = "0122w000000HqWe";
        
        function getCookies(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
      
        function checkCookies() {
            var supportemailCookie=getCookies("supportemail");
            var supportnameCookie=getCookies("supportname");
            var supportphoneCookie=getCookies("supportphone");
            var identifybyCookie=getCookies("identifyby");
            var supportsiteidCookie=getCookies("supportsiteid");
            var supportstorenumberCookie=getCookies("supportstorenumber");
            var supportbrandCookie = getCookies("supportbrand");
            var sitephoneCookie = getCookies("sitephone");
            
            if (supportemailCookie != "" || supportnameCookie != "" || supportphoneCookie || identifybyCookie != "" || supportsiteidCookie != "" || supportstorenumberCookie != "" || supportbrandCookie != "" || sitephoneCookie != "") {
                
                document.getElementById("email").value = supportemailCookie;
                document.getElementById("name").value = supportnameCookie;
                document.getElementById("phone").value = supportphoneCookie;
				
                // Set Values of radio button - Starts here
                var identifybyRadio = document.getElementsByName('00N2w000005hpKM');
                for(var i = 0; i < identifybyRadio.length; i++){
                    if(identifybyRadio[i].value == identifybyCookie){
                        identifybyRadio[i].checked = true;
                    }
                }
                // Set Values of radio button - Ends here
                document.getElementById("00N2w000005hpKO").value = supportsiteidCookie;
                document.getElementById("00N2w000005hpKe").value = supportstorenumberCookie;
                document.getElementById("00N2w000005hpKG").value = supportbrandCookie;
                document.getElementById("00N2w000005hpKd").value = sitephoneCookie;
            } else {
                
            }
        }
        checkCookies();
        
        function setIdentifyBy(){
        var identifybyCookieVar = getCookies("identifyby");
        var supportsiteidCookieVar = getCookies("supportsiteid");
        var supportstorenumberCookieVar = getCookies("supportstorenumber");
        var supportbrandCookieVar = getCookies("supportbrand");
        var sitephoneCookieVar = getCookies("sitephone");
        if(identifybyCookieVar == 'Invenco Site ID'){
            document.getElementById('getbysiteid').style.display ='block';
            document.getElementById('00N2w000005hpKO').value = supportsiteidCookieVar;
            document.getElementById('getbystorenumber').style.display ='none';
            document.getElementById('getbybrand').style.display ='none';
            document.getElementById('getbysitephone').style.display ='none'; 
        }
        else if (identifybyCookieVar == 'Store Number'){
            document.getElementById('getbysiteid').style.display ='none';
            document.getElementById('getbystorenumber').style.display ='block';
            document.getElementById('00N2w000005hpKe').value = supportstorenumberCookieVar;
            document.getElementById('getbybrand').style.display ='block';
            document.getElementById('00N2w000005hpKG').value = supportbrandCookieVar;
            document.getElementById('getbysitephone').style.display ='none';
        }
        else if(identifybyCookieVar == 'Site Phone Number'){
            document.getElementById('getbysiteid').style.display ='none';
            document.getElementById('getbystorenumber').style.display ='none';
            document.getElementById('getbybrand').style.display ='none';
            document.getElementById('getbysitephone').style.display ='block';
            document.getElementById('00N2w000005hpKd').value = sitephoneCookieVar;
        }
        else {
            document.getElementById('getbysiteid').style.display ='none';
            document.getElementById('getbystorenumber').style.display ='none';
            document.getElementById('getbybrand').style.display ='none';
            document.getElementById('getbysitephone').style.display ='block';
            document.getElementById("00N2w000005hpKd").required = true;
        }
        }
		setIdentifyBy();
        
    },
    setTechnicalCase: function(cmp){   
        document.getElementById('fuelingpositions').style.display ='block';
        document.getElementById('issuetypereported').style.display ='block';
        document.getElementById("00N2w000005hpKL").required = true;
        document.getElementById("00N2w000005hpKQ").required = true;
        document.getElementById('summary').style.display ='none';
        document.getElementById("subject").required = false;
        document.getElementById('recordType').value = "0122w000000HqWe";
    },
    setAccountCase: function(cmp){
        document.getElementById('fuelingpositions').style.display ='none';
        document.getElementById('00N2w000005hpKL').value = "";
        document.getElementById("00N2w000005hpKL").required = false;
        document.getElementById('issuetypereported').style.display ='none';
        document.getElementById('00N2w000005hpKQ').value = "";
        document.getElementById("00N2w000005hpKQ").required = false;
        document.getElementById('summary').style.display ='block';
        document.getElementById("subject").required = true;
        document.getElementById('recordType').value = "0122w000000HqWd";
    },
    setIdentifyBy: function(cmp){
        var identifybyRadio = document.getElementsByName('00N2w000005hpKM');
        var identifybyValue;
        for(var i = 0; i < identifybyRadio.length; i++){
            if(identifybyRadio[i].checked){
                identifybyValue = identifybyRadio[i].value;
            }
        }
        if(identifybyValue == 'Invenco Site ID'){
            document.getElementById('getbysiteid').style.display ='block';
            document.getElementById("00N2w000005hpKO").required = true;
            
            document.getElementById('getbystorenumber').style.display ='none';
            document.getElementById("00N2w000005hpKe").required = false;
            
            document.getElementById('getbybrand').style.display ='none';
            document.getElementById("00N2w000005hpKG").required = false;
            
            document.getElementById('getbysitephone').style.display ='none';
            document.getElementById("00N2w000005hpKd").required = false;
        }
        else if (identifybyValue == 'Store Number'){
            document.getElementById('getbysiteid').style.display ='none';
            document.getElementById("00N2w000005hpKO").required = false;
            
            document.getElementById('getbystorenumber').style.display ='block';
            document.getElementById("00N2w000005hpKe").required = true;
            
            document.getElementById('getbybrand').style.display ='block';
            document.getElementById("00N2w000005hpKG").required = true;
            
            document.getElementById('getbysitephone').style.display ='none';
            document.getElementById("00N2w000005hpKd").required = false;
        }
        else if(identifybyValue == 'Site Phone Number'){
            document.getElementById('getbysiteid').style.display ='none';
            document.getElementById("00N2w000005hpKO").required = false;
            
            document.getElementById('getbystorenumber').style.display ='none';
            document.getElementById("00N2w000005hpKe").required = false;
            
            document.getElementById('getbybrand').style.display ='none';
            document.getElementById("00N2w000005hpKG").required = false;
            
            document.getElementById('getbysitephone').style.display ='block';
            document.getElementById("00N2w000005hpKd").required = true;
        }
        else {}
    }
})