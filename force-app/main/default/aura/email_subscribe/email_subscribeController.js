({
	keyCheck : function(component){
        var str = document.getElementById("email-subscribe").value;
        var at = str.search("@");
        var lastname = str.substring(0, at);
        console.log(lastname);
   		component.set('v.user_lastname', lastname);
        
        //This part is to fetch company name after @ sign in email
        //var len = str.length;
        //var str2 = str.substring((at+1), len);
        //var dot = str2.indexOf(".");
        //var company = str2.substring(0, dot);
        //component.set('v.user_company', company); 
      },
      setCookie : function(cmp){
        var d = new Date();
        var exdays = 30;
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString(); 
        var subscribedemail = document.getElementById("email-subscribe").value;   
        document.cookie = "subscribedemail=" + subscribedemail + ";" + expires + "; path=/; secure; samesite=strict; true";
      },
      deleteCookie: function(cmp){
        document.cookie = "subscribedemail= ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; secure; samesite=strict; true";
      },
      render: function(cmp){
          function getCookie(cname) {
              var name = cname + "=";
              var decodedCookie = decodeURIComponent(document.cookie);
              var ca = decodedCookie.split(';');
              
              for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                  c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
                }
              }
              return "";
            }
            
            function checkCookie() {
              var targetCookie=getCookie("subscribedemail");
              
              if (targetCookie != "") {
                document.getElementById("email-subscribe").value = targetCookie;
              } else {
                 
              }
            }
            
            checkCookie();
        }

})