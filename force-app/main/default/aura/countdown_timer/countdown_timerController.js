({ 
    init : function(cmp) {
	  var deadline = cmp.get("v.deadline");
      var countDownDate = new Date(deadline).getTime();
      
      var x = setInterval(function() {
    
      // Get today's date and time
      var now = new Date().getTime();
    
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
    
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      cmp.set('v.days', days);
      cmp.set('v.hours', hours); 
      cmp.set('v.minutes', minutes); 
      cmp.set('v.seconds', seconds);  
    
    }, 1000); 
    },
})