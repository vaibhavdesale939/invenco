({
	doInit : function(component, event, helper) {
        
         var recID = component.get("v.recordId");
         var action = component.get('c.doInit');
         var action = component.get("c.fecthExistingValues");
         action.setParams({
            recordId :  recID
        });
		
         action.setCallback(this, function(response){
            var state = response.getState(); 
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); 
                
                var array = [];
                for(var i = 0; i < result.length; i++){
                    array= result[i];

              }
               
                
               
                var accId =array.AccountId ;
               
                component.set("v.AccountId",accId); 
                var conId = array.ContactId;
                
                component.set("v.ContactId",conId);
                var subject = array.Subject;
               
                component.set("v.Subject",subject);
                var desc = array.Description;
                
                component.set("v.Description",desc);
                var origin = array.Origin;
                
                component.set("v.Origin",origin);
                var currentUserProfileId = array.LoggedUserProfileName__c;
               
                component.set("v.LoggedInUserProfileId",currentUserProfileId);

               
            }
            else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
               
                if (errors) {
                    alert("Error message: " + 
                          errors[0].message);
                } else {
                    alert("Unknown error");
                }
            }
            
        });
        
          $A.enqueueAction(action); 
        
	},
    
     handleSubmit : function(component, event, helper) {
        event.preventDefault(); // Prevent default submit
        var record = component.get("v.recordId");
        var fields = event.getParam("fields");
        var isFinanceCase = component.get("v.IsOpen");
        
       
        if(isFinanceCase == true){

            fields["RecordTypeId"] = component.get("v.FinanceRecordtypeId");
            fields["OwnerId"] = component.get("v.FinanceQueueId"); 
        }
       

        var payload = event.getParams("fields");
        var objJSON = JSON.parse(JSON.stringify(payload.fields));
        
        component.find('createCase').submit(fields); // Submit form

       
        
     },
    
     handleSuccess: function(cmp, event, helper) {
        var params = event.getParams();
        cmp.set('v.recordId', params.response.id);
        cmp.set('v.showSpinner', false);
        cmp.set('v.saved', true);
        var recordInformation = event.getParam("response");
        
    },
    
    
    callSuccess : function(component, event, helper) {
       
        var payload = event.getParams().response;
       
        var caseNumber = payload.fields.CaseNumber.value;
        var recordInformation = event.getParam("response");
        
        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Success!",
                            message: "Account Case is Created.",
                            messageTemplate: '{1} is Created.',
                            messageTemplateData: ['Salesforce', {
                                url: '/' + payload.id ,
                                label: 'Case -' + caseNumber ,
                            }],
                            type: "success",
                            duration:"10000",   
                        });
                       
                        toastEvent.fire();

                        component.set("v.IsOpen",false);
                        component.set("v.IsOpenRepairCase",false);                 
    },
    
    openModal : function(component,event,helper){
        var action = component.get('c.doInit');
        $A.enqueueAction(action);
        component.set("v.IsOpen",true);
    },

    closemodal : function(component,event,helper){
        //$A.get("e.force:closeQuickAction").fire();
        component.set("v.IsOpen",false);
    },

   
   ShowHideAllFinance: function (cmp, event) {
        let activeSections  = cmp.get("v.activeSections");

        if (activeSections.length === 0) {
            cmp.set("v.activeSections",["CaseInformation","DescriptionInformation"]);
        } else {
            cmp.set("v.activeSections",["CaseInformation"]);
        }
    }


})