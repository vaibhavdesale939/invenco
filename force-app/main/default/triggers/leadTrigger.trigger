/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Lead Trigger for lead assignment rule
Test class:     leadTriggerTest

History
01-02-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
trigger leadTrigger on Lead (after insert) {

     if(!PreventRunningLogic__c.getInstance(System.UserInfo.getUserId()).DontRunLogic__c){
        LeadTriggerHandler.handleMethod(Trigger.new);
    }

}