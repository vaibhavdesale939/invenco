/**************************************************************************************************************************************
* Apex Trigger: WOLineItemTrigger
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* - Trigger on Work Order Line Object
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
    1.0         Vaibhav D               FEB 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/


trigger WOLineItemTrigger on WorkOrderLineItem (after insert,after update,after delete,after undelete) {
    
if(!PreventRunningLogic__c.getInstance(System.UserInfo.getUserId()).DontRunLogic__c){    
    TriggerDispatcher.Run(new WorkOrderLineItemTriggerHandler());   
    
}
}