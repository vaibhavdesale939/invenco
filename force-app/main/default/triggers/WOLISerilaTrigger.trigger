/**************************************************************************************************************************************
* Apex Trigger: WOLISerilaTrigger
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* - Trigger on Work Order Line Serial Number Object
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
1.0         Vaibhav D               March 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

trigger WOLISerilaTrigger on Work_Order_Line_Serial__c (after insert,after update,after delete,after undelete) {
    
    if(!PreventRunningLogic__c.getInstance(System.UserInfo.getUserId()).DontRunLogic__c){    
        TriggerDispatcher.Run(new WOLISerialTriggerHandler());   
    }
    
}