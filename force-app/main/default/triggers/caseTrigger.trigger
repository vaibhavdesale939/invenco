/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Case Trigger for web to case subject
Test class:     caseTriggerTest

History
11-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
trigger caseTrigger on Case (before insert) {

     if(!PreventRunningLogic__c.getInstance(System.UserInfo.getUserId()).DontRunLogic__c){
        CaseTriggerHandler.handleMethod(Trigger.new);
    }

}