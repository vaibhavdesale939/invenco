global class MilestoneTimeCalculatorForProRepairSLA implements Support.MilestoneTriggerTimeCalculator {
    global integer calculateMilestoneTriggerTime(String WoId, String milestoneID){
        
        Integer TimeTrigger;
        
        
        //create a Map of Day and EndBusineesTimeofDay
        map<string,time>mapEndTimeofDay = new map<string,time>();
        
        
        try{
            //itereate on WO List
            for(WorkOrder wo : [SELECT Id,
                                SlaStartDate, 
                                BusinessHoursId,
                                BusinessHours.MondayEndTime,
                                BusinessHours.TuesdayEndTime,
                                BusinessHours.WednesdayEndTime,
                                BusinessHours.ThursdayEndTime,
                                BusinessHours.FridayEndTime,
                                BusinessHours.SaturdayEndTime,
                                BusinessHours.SundayEndTime
                                FROM WorkOrder 
                                WHERE Id=:WoId]){
                                    
                                    //Put Key as WO Id and Day and Values as BusinessHourEndTime
                                    mapEndTimeofDay.put(Wo.Id +'-Monday',wo.BusinessHours.MondayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Tuesday',wo.BusinessHours.TuesdayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Wednesday',wo.BusinessHours.WednesdayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Thursday',wo.BusinessHours.ThursdayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Friday',wo.BusinessHours.FridayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Saturday',wo.BusinessHours.SaturdayEndTime);
                                    mapEndTimeofDay.put(Wo.Id +'-Sunday',wo.BusinessHours.SundayEndTime);
                                    
                                    if(wo.SlaStartDate != null){
                                        
                                        //get SLA Start Date
                                        Datetime SLATimeOnWo = wo.SlaStartDate;
                                        
                                        //get SLA Start Date as per User's Time zone
                                        Integer offset = UserInfo.getTimezone().getOffset(SLATimeOnWo);
                                        
                                        Datetime SLATimeOnWoAsLoggedInUser = SLATimeOnWo.addSeconds(offset/1000);
                                        
                                        //function to get Day from SLA Start Date
                                        DateTime myDateTime = (DateTime) SLATimeOnWoAsLoggedInUser;
                                        system.debug('myDateTime'+myDateTime.dateGmt());
                                        date SLADateonWO = myDateTime.dateGmt();
                                        date SLADateonWOasUser = SLADateonWO.addDays(offset/ 86400000 );
                                        system.debug('SLADateonWO'+SLADateonWO);
                                        
                                        datetime dt = myDateTime.dateGmt();
                                        String SLADateDayOnWO = dt.format('EEEE');
                                        
                                        //create a composite key with WO id and Day of SLA Start Date to get END Time related to particular Day
                                        string mapUniqueKey = Wo.Id + '-' + SLADateDayOnWO;
                                        
                                        //get Business End Time from Map with Composite Key
                                        Time BusinessHourEndtimeOfDay = mapEndTimeofDay.get(mapUniqueKey);
                                        
                                        //Add SLA Start Date and Business Hour End Time to Form a DateTime
                                        DateTime BusinessHourEndTimeofWO  = DateTime.newInstance(SLADateonWO, BusinessHourEndtimeOfDay);
                                        
                                        //Format the Concatenate DateTime 
                                        string formatendtimeofDay = BusinessHourEndTimeofWO.format('YYYY-MM-dd HH:mm:ss');
                                        DateTime endTimeofBusiness = DateTime.valueof(formatendtimeofDay);
                                        
                                        //Get Concatenate DateTime in Logged in User's Format 
                                        Datetime BusinessEndTimeAsUser = endTimeofBusiness.addSeconds(offset/1000);
                                        
                                        //find Next Business Day's Business Start Time
                                        Datetime nextDayStarTimeofBusiness = BusinessHours.nextStartDate(wo.BusinessHoursId, BusinessEndTimeAsUser);
                                        
                                        //get Next Business Day's Business Start Time in Logged in User's Format
                                        Datetime nextDayStarTimeofBusinessAsUser = nextDayStarTimeofBusiness.addSeconds(offset/1000);
                                        
                                        //function to Get Day from Next Business Day's Business Start Date
                                        DateTime NextDayBussinessDate = (DateTime) nextDayStarTimeofBusinessAsUser;
                                        
                                        String NextdayOfBusiness = NextDayBussinessDate.format('EEEE');
                                        
                                        //create a composite key for Map to get Business End Time of Next Business Day
                                        string nextdayMatchKey = Wo.Id + '-' + NextDayOfBusiness;
                                        
                                        //Get Date Value from the Next Business Day's Start Date
                                        date NextDateofBusiness = nextDayStarTimeofBusinessAsUser.date();
                                        
                                        
                                        //get the value of Business End Time from Map
                                        Time nextDayBusinessFinishHours = mapEndTimeofDay.get(nextdayMatchKey);
                                        
                                        //Add the Finish Time in Next Day's Business Start Date
                                        DateTime endtimeofDay1  = DateTime.newInstance(NextDateofBusiness, nextDayBusinessFinishHours);
                                        
                                        //Format Concatenate DateTime
                                        string formatEndTimeofNextBusinessDay = endtimeofDay1.format('YYYY-MM-dd HH:mm:ss');
                                        DateTime endTimeofNextBusinessDay = DateTime.valueof(formatEndTimeofNextBusinessDay);
                                        
                                        //Convert Concatenate DateTime Logged in User's Timezone
                                        Datetime endTimeofNextBusinessDayAsUser = endTimeofNextBusinessDay.addSeconds(offset/1000);
                                        
                                        
                                        //calculate the Difference between remaining Time for SLA from SLA start Time
                                        Decimal remaoningBusinesshoursforSLA = (BusinessHours.diff (Wo.BusinessHoursId, wo.SlaStartDate, endTimeofNextBusinessDayAsUser ) / 60000);
                                        
                                        Decimal remainingBusinessminutesforSLA = remaoningBusinesshoursforSLA.round();
                                        
                                        //assign remaining minutes and return to Milestone Trigger
                                        TimeTrigger = integer.valueof(remainingBusinessminutesforSLA);
                                        
                                        
                                        
                                        return TimeTrigger;
                                    }
                                }
        }
        
        catch (exception e){
            SaveResult result = new SaveResult().addError(e.getMessage())
                .addErrorToLog(e,null,WoId,'MilestoneTimeCalculatorForProRepairSLA','calculateMilestoneTriggerTime');
            result.getReturnValue();
        }
        return 1;
    }
}