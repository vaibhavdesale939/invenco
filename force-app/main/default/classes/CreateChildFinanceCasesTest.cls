@isTest
public class CreateChildFinanceCasesTest {
    
    @testSetup static void setupTestData(){
        
        List<Account>accLst = new List<Account>();
        
        TestDataFactory.AccountParams AParms = new TestDataFactory.AccountParams();
        AParms.Name = 'Arco';
        AParms.ServicePartner = True;
        AParms.billingstreet = 'Santa Clara St';
        AParms.billingstate = '';
        
        List<Account> acclist1 = TestDataFactory.createTestAccount(1,AParms);
        accLst.addAll(acclist1);
        
        insert accLst;
        
        List<Case>csLst = new List<Case>();
        
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Subject = 'Payment not Received';
        cParms.Accountid = acclist1[0].Id;
        cParms.Status = 'New';
        cParms.Origin = 'Email';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        csLst.addAll(cslist1);
        
        insert csLst;
   }
    
      @isTest static void test_fecthExistingValues(){
		 List<Case> CaseList = [SELECT Id,AccountId,Subject,Status
                                            FROM Case];
        Test.startTest();
        CreateChildFinanceCases.fecthExistingValues(CaseList[0].id);
        Test.stopTest();
	}

}