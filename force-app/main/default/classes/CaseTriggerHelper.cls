/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Case Trigger Helper class for Case trigger

History
11-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
public class CaseTriggerHelper {
    
    public static void handleCaseAssignment(List<Case> newCaseList){
        
        List<Case> Caselist= new List<Case>();
        List<Case> listCase = new List<Case>();
        try{
            for (Case thisCase : newCaseList) {
                String Technicalrecordtypelabel= System.Label.TechnicalRecordTypeId;
                if (thisCase.Origin == 'Web' && thisCase.RecordTypeId == Technicalrecordtypelabel) {
                   List <string> tmpString = new List<String> ();
                    //String[] picklistValues =thisCase.IssueTypeReported__c.split(';');
                    tmpString.addall(thisCase.IssueTypeReported__c.split(';'));
					tmpString.sort();
                   	thisCase.Subject = tmpString[0] + ':' + thisCase.FuelingPositions__c + ':' + thisCase.SuppliedName; 
                   	Caselist.add(new Case(id = thisCase.id , Subject=thisCase.Subject ));
              }
              if(!Caselist.isEmpty()){update Caselist;}
          }
       }
        Catch(Exception ex){
            SaveResult result = new SaveResult().addError(ex.getMessage())
                .addErrorToLog(ex,null,null,'CaseTriggerHelper','handleCaseAssignment');
            result.getReturnValue();
        }
    }
}