/**************************************************************************************************************************************
* Apex Class: WOLISerialTriggerHandler
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* - Handle Trigger Events and call trigger actions.
1.To Update WorkOrderSerial Count from WO Line Item Serial Number on Work Order Line Item.
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
    1.0         Vaibhav D               FEB 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

public class WOLISerialTriggerHandler  implements ITriggerHandler{
    
    public void AfterInsert(Map<Id, SObject> newItems){
        
        List<Work_Order_Line_Serial__c>WOLIserial = newItems.values();
        WOLISerialTriggerHelper.updateSerialCountonWOLI(WOLIserial);
        
    }    
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        
        List<Work_Order_Line_Serial__c>WOLIserial = newItems.values();
        WOLISerialTriggerHelper.updateSerialCountonWOLI(WOLIserial);
        
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems){
        
        List<Work_Order_Line_Serial__c>WOLIserial = oldItems.values();
        WOLISerialTriggerHelper.updateSerialCountonWOLI(WOLIserial);
        
    }    
    
    public void AfterUnDelete( Map<Id, SObject> newItems){
        
        List<Work_Order_Line_Serial__c>WOLIserial = newItems.values();
        WOLISerialTriggerHelper.updateSerialCountonWOLI(WOLIserial);
    }


}