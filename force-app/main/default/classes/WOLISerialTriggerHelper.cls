/**************************************************************************************************************************************
* Apex Class: WOLISerialTriggerHelper
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
1.To Update WorkOrderSerial Count from WO Line Item Serial Number on Work Order Line Item.
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
1.0         Vaibhav D               March 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

public class WOLISerialTriggerHelper {
    
    public static void updateSerialCountonWOLI(list<Work_Order_Line_Serial__c> WOLIserial){
        Set<Id> WOLISerilaId= new Set<Id>();
        List<WorkOrderLineitem>WOLIlsttoUpdate = new List<WorkOrderLineItem>();
        
        for(Work_Order_Line_Serial__c WOLISer : WOLIserial){
            if(WOLISer.WorkOrderLineItem__c != Null){
                WOLISerilaId.add(WOLISer.WorkOrderLineItem__c);
            }
        } 
        
        try{
            for(WorkOrderLineItem woli :[select Id, WOLISerialCount__c ,(select id,WorkOrderLineItem__c from Work_Order_Line_Serials__r) from WorkOrderLineItem where Id IN: WOLISerilaId])
            {
                decimal count = 0;
                
                for(Work_Order_Line_Serial__c woliserials : woli.Work_Order_Line_Serials__r )
                {
                    count += 1;
                }
                
                
                woli.WOLISerialCount__c = count;
                WOLIlsttoUpdate.add(woli);
            }
            
            if(!WOLIlsttoUpdate.isEmpty())
            {
                update WOLIlsttoUpdate;
            }
            
        }
        
        catch (exception e){
            SaveResult result = new SaveResult().addError(e.getMessage())
                .addErrorToLog(e,null,null,'WOLISerialTriggerHelper','updateSerialCountonWOLI');
            result.getReturnValue();
        }
    }
}