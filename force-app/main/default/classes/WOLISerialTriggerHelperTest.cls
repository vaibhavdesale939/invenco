/**************************************************************************************************************************************
* Apex Class: WOLISerialTriggerHelperTest
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
1.Test Class for Work Order Serial Trigger
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
1.0         Vaibhav D               March 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

@isTest
public class WOLISerialTriggerHelperTest {
 @testSetup static void setupTestData(){
        
        List<Account>accLst = new List<Account>();
        
        TestDataFactory.AccountParams AParms = new TestDataFactory.AccountParams();
        AParms.Name = 'Arco';
        AParms.ServicePartner = True;
        AParms.billingstreet = 'Santa Clara St';
        AParms.billingstate = '';
        
        List<Account> acclist1 = TestDataFactory.createTestAccount(1,AParms);
        accLst.addAll(acclist1);
         
        insert accLst;
        
        List<Case>csLst = new List<Case>();
        
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Subject = 'Payment not Received';
        cParms.Accountid = acclist1[0].Id;
        cParms.Status = 'New';
        cParms.Origin = 'Email';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        csLst.addAll(cslist1);
        
        insert csLst;
        
        List<WorkOrder>woLst = new List<WorkOrder>();
        
        TestDataFactory.WoParams woParms = new TestDataFactory.WoParams();
        woParms.Subject = 'Payment not Received';
        woParms.Accountid = acclist1[0].Id;
        woParms.Status = 'New';
        woParms.Caseid = cslist1[0].id;
        
        List<WorkOrder> wolist1 = TestDataFactory.createTestWorkOrder(1,woParms);
        woLst.addAll(wolist1);
        
        insert woLst;
        
        
        List<WorkOrderLineItem>woLiLst = new List<WorkOrderLineItem>();
        
        TestDataFactory.WoLiParams woLiParms = new TestDataFactory.WoLiParams();
        woLiParms.Location = 'CA';
        woLiParms.WorkOrderId = wolist1[0].Id;
        woLiParms.Status = 'New';
        
        List<WorkOrderLineItem> woLilst1 = TestDataFactory.createTestWorkOrderLines(3,woLiParms);
        woLiLst.addAll(woLilst1);
        
        insert woLiLst;
     
        List<Work_Order_Line_Serial__c>  woliserilalst = new list<Work_Order_Line_Serial__c>();
        TestDataFactory.WOLISerialParams woLisParms = new TestDataFactory.WOLISerialParams();
        woLisParms.Location = 'CA';
        woLisParms.WOLIid = woLilst1[0].Id;
        woLisParms.serialNumber = 'New';
        
        List<Work_Order_Line_Serial__c> woSerLilst1 = TestDataFactory.createWOLISerialNumber(3,woLisParms);
        woSerLilst1.addAll(woliserilalst);
     
        insert woSerLilst1;
    }
    
    
    @isTest static void test_fecthExistingValues(){
        map<id,Work_Order_Line_Serial__c> woLimap = new map<id,Work_Order_Line_Serial__c> ([SELECT Id,WorkOrderLineItem__c,NewSerialNumber__c,Location__c
                                                                            FROM Work_Order_Line_Serial__c]);
        Test.startTest();
        
        WOLISerialTriggerHandler wolihander = new WOLISerialTriggerHandler();
        wolihander.AfterInsert(woLimap);
        
        WOLISerialTriggerHandler wolihander1 = new WOLISerialTriggerHandler();
        wolihander1.AfterDelete(woLimap);
        WOLISerialTriggerHandler wolihander2 = new WOLISerialTriggerHandler();
        wolihander2.AfterUnDelete(woLimap);
        WOLISerialTriggerHandler wolihander3 = new WOLISerialTriggerHandler();
        wolihander3.AfterUpdate(woLimap, woLimap);
        
        
        Test.stopTest();
    }
    
}