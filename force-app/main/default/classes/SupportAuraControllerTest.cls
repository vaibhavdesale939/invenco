/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    SupportAuraController Test class


History
20-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
private class SupportAuraControllerTest {
    
    @testSetup static void setupTestData(){
       
        
        TestDataFactory.AccountParams aParms = new TestDataFactory.AccountParams();
        aParms.Name= 'Test Account';
        aParms.ServicePartner=TRUE;
        aParms.billingstreet ='Santa Clara St';
        aParms.billingstate= '';
        List<Account> acclist = TestDataFactory.createTestAccount(1,aParms);
		insert acclist;
        
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.AccountId= acclist[0].id;
        cParms.Subject='';
        cParms.status = 'New';
        cParms.origin= 'Web';
        List<Case> caselist = TestDataFactory.createTestCases(1,cParms);
        caselist[0].SuppliedName='Saint Clara';
        caselist[0].Brand__c='Alon';
        caselist[0].FuelingPositions__c= 'POS12';
        caselist[0].IssueTypeReported__c='Barcode Scanner Card Types ; Card reader flashing lights';
		insert caselist;
            
                
    }
    
    @isTest static void test_getPiklistValues(){
        List <Case> Caselist= new List<Case>([SELECT Id,Origin,Subject,FuelingPositions__c,IssueTypeReported__c,Brand__c,SuppliedName
                                              FROM Case]);
        Test.startTest();
        SupportAuraController.getPiklistValues();
        Test.stopTest();
    }
    
     @isTest static void test_getIssuePiklistValues(){
        List <Case> Caselist= new List<Case>([SELECT Id,Origin,Subject,FuelingPositions__c,IssueTypeReported__c,SuppliedName
                                              FROM Case]);
        Test.startTest();
        SupportAuraController.getIssuePiklistValues();
        Test.stopTest();
    }
    
}