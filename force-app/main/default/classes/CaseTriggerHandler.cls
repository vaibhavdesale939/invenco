/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Case Trigger Handler for Case Trigger


History
11-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
public class CaseTriggerHandler{

     public static void handleMethod(List<Case> newCaseList){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                CaseTriggerHelper.handleCaseAssignment(newCaseList);
          }
        }
    }
}