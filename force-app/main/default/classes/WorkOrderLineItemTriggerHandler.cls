/**************************************************************************************************************************************
* Apex Class: WorkOrderLineItemTriggerHandler
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* - Handle Trigger Events and call trigger actions.
1.To Update Locations from WO Line Item on Work Order. The Locations are used in  email templates sent to customer
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
    1.0         Vaibhav D               FEB 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/


public class WorkOrderLineItemTriggerHandler implements ITriggerHandler  {
    
    public void AfterInsert(Map<Id, SObject> newItems){
        
        List<WorkOrderLineItem>wolilst = newItems.values();
        WorkOrderLineItemHelper.updateLocationOnWorkOrder(wolilst);
        
    }    
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        
        List<WorkOrderLineItem>wolilst = newItems.values();
        WorkOrderLineItemHelper.updateLocationOnWorkOrder(wolilst);
        
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems){
        
        List<WorkOrderLineItem>wolilst = oldItems.values();
        WorkOrderLineItemHelper.updateLocationOnWorkOrder(wolilst);
        
    }    
    
    public void AfterUnDelete( Map<Id, SObject> newItems){
        
        List<WorkOrderLineItem>wolilst = newItems.values();
        WorkOrderLineItemHelper.updateLocationOnWorkOrder(wolilst);
    }
    
}