/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Lead Trigger Handler for Lead Trigger


History
01-02-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
public class LeadTriggerHandler{

     public static void handleMethod(List<Lead> newLeadList){
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                LeadTriggerHelper.handleLeadAssignment(newLeadList);
          }
        }
    }
}