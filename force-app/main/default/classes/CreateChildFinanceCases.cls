/**************************************************************************************************************************************
* Apex Class: CreateChildFinanceCases
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
1.Get the Data from Parent case and provide to CreateChildCase component to pre-populate certian fields.
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
1.0         Vaibhav D               Jan 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

public class CreateChildFinanceCases {
    
    @AuraEnabled
    public static List<Case> fecthExistingValues (string recordId ){
        
        List<Case>lstCaseDetails =  new List<Case>();
       //get the parent case details and send to component
        lstCaseDetails = [Select Id,
                         		AccountId,
                         		ContactId,
                         		Subject,
                         		Description,
                         		Origin,
								LoggedUserProfileName__c
								 
                          		From Case
                         		Where Id =: recordId];

           

		
        return lstCaseDetails;
    }

}