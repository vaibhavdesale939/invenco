/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    RegisterAuraController Apex class Test class


History
20-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
private class RegisterAuraControllerTest {
    
    @testSetup static void setupTestData(){
        List<Lead> leadlist = new  List<Lead>();
     
        TestDataFactory.LeadParams l2Parms = new TestDataFactory.LeadParams();
        l2Parms.FirstName = 'Test first name';
        l2Parms.LastName ='Test last name';
        l2Parms.Company = 'Test company'; 
        l2Parms.Status = 'Unqualified'; 
        l2Parms.LeadSource = 'Trade Show';
        List<Lead> leadlist2 = TestDataFactory.createTestLeads(1,l2Parms);
        leadlist2[0].Assignusingactiveassignmentrule__c=FALSE;
        leadlist2[0].Brand__c='Alon';
        leadlist.addAll(leadlist2);
        insert leadlist;
    }
    
    @isTest static void test_getPicklistvalues(){
        List <Lead> Leadlist= new List<Lead>([SELECT Id,Company,LeadSource,Status,Brand__c
                                              FROM Lead]);
        Test.startTest();
        RegisterAuraController.getPiklistValues();
        Test.stopTest();
    }
    
}