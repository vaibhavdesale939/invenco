/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Test data factory for master data
History
01-02-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class TestDataFactory{
    //Lead Test Data ====================
    public class LeadParams{
        public String LastName;
        public String FirstName;
        public String Company;
        public String Status;
        public String LeadSource;
        
    }
    
    public static List<Lead> createTestLeads(Integer numOfRecords,LeadParams ldParams){
        //Fetch record types
        
        List<Lead> leadsRecords = new List<Lead>();
        for(Integer count=0;count<numOfRecords;count++){
            Lead leadRec = new Lead();
            leadRec.FirstName = ldParams.FirstName; 
            leadRec.LastName = ldParams.LastName; 
            leadRec.Company = ldParams.Company; 
            leadRec.Status = ldParams.Status; 
            leadRec.LeadSource = ldParams.LeadSource; 
            leadsRecords.add(leadRec);
        }
        return leadsRecords;
    }
    
    
    
    //Account Test Data ====================
    public class AccountParams{
        public String Name;
        public boolean ServicePartner;
        public string  billingstreet;
        public string  billingstate;
       
    }
    
    public static List<Account> createTestAccount(Integer numOfRecords,AccountParams accparams){
        //Fetch record types
        Id recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site').getRecordTypeId();
   
        List<Account> AccountRecords = new List<Account>();
        for(Integer count=0;count<numOfRecords;count++){
            Account AccRec = new Account();
            AccRec.Name = accparams.Name; 
            AccRec.ServicePartner__c = accparams.ServicePartner; 
            AccRec.BillingStreet = accparams.billingstreet; 
            AccRec.BillingState = accparams.billingstate; 
            AccRec.RecordTypeId = recordtypeId; 
            AccountRecords.add(AccRec);
        }
        return AccountRecords;
    }
    
    //Case Test Data ====================
    public class CaseParams{
        public String AccountId;
        public string Subject;
        public string  status;
        public string  origin;
    }
    
    public static List<Case> createTestCases(Integer numOfRecords,CaseParams csparams){
        //Fetch record types
        
        Id recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical').getRecordTypeId();
        
        List<Case> CaseRecords = new List<Case>();
        for(Integer count=0;count<numOfRecords;count++){
            Case cs = new Case();
            cs.AccountId = csparams.AccountId; 
            cs.Subject = csparams.Subject; 
            cs.status = csparams.status; 
            cs.origin = csparams.origin; 
            cs.RecordtypeId = recordtypeId;
            CaseRecords.add(cs);
        }
        return CaseRecords;
    }
    
    //Work Order Test Data ====================
    public class WoParams{
        public String AccountId;
        public string Subject;
        public string  status;
        public string  CaseId;
        public datetime sla;
        public id businesshour;
        
    }
    
    public static List<WorkOrder> createTestWorkOrder(Integer numOfRecords,WoParams WoParam){
        //Fetch record types
        Id recordtypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        
        
        List<WorkOrder> WORecords = new List<WorkOrder>();
        for(Integer count=0;count<numOfRecords;count++){
            WorkOrder wo = new WorkOrder();
            wo.AccountId = WoParam.AccountId; 
            wo.Subject = WoParam.Subject; 
            wo.status = WoParam.status; 
            wo.CaseId = WoParam.CaseId; 
            wo.RecordTypeId = recordtypeId;
            wo.SlaStartDate = woParam.sla;
            wo.BusinessHoursId = woParam.businesshour;
            WORecords.add(wo);
        }
        return WORecords;
    }
    
    //Work Order Lien Item Test Data ====================
    public class WoLiParams{
        public String Status;
        public string WorkOrderId;
        public string  Location;
       
        
        
    }
    
    public static List<WorkOrderLineItem> createTestWorkOrderLines(Integer numOfRecords,WoLiParams WoLiParam){
        //Fetch record types
        
        Id recordtypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        
        List<WorkOrderLineItem> WOLiRecords = new List<WorkOrderLineItem>();
        for(Integer count=0;count<numOfRecords;count++){
            WorkOrderLineItem woLi = new WorkOrderLineItem();
            woLi.Status = WoLiParam.Status; 
            woLi.WorkOrderId = WoLiParam.WorkOrderId; 
            woLi.Location__c = WoLiParam.Location; 
            woLi.recordTypeId = recordtypeId;
           
            WOLiRecords.add(woLi);
        }
        return WOLiRecords;
    }
    
    
     //Work Order Lien Item Test Data ====================
    public class MilestoneParams{
        public String caseId;
       
        
        
    }
    
     public static List<CaseMilestone> createCaseMilestone(Integer numOfRecords,MilestoneParams csParam){
        //Fetch record types
        
       
        
        List<CaseMilestone> casemilerecs = new List<CaseMilestone>();
        for(Integer count=0;count<numOfRecords;count++){
            CaseMilestone csm = new CaseMilestone();
          
            casemilerecs.add(csm);
        }
        return casemilerecs;
    }
    
     //Work Order Lien Item Test Data ====================
    public class WOLISerialParams{
        public String WOLIid;
        public string location;
        public string serialNumber;
    }
    
     public static List<Work_Order_Line_Serial__c> createWOLISerialNumber(Integer numOfRecords,WOLISerialParams wolisparam){
        //Fetch record types
        
       
        
        List<Work_Order_Line_Serial__c> woliserila = new List<Work_Order_Line_Serial__c>();
        for(Integer count=0;count<numOfRecords;count++){
            Work_Order_Line_Serial__c wolis = new Work_Order_Line_Serial__c();
            wolis.NewSerialNumber__c = wolisparam.serialNumber;
            wolis.Location__c = wolisparam.location;
            wolis.WorkOrderLineItem__c = wolisparam.WOLIid;
            woliserila.add(wolis);
        }
        return woliserila;
    }
    
}