@isTest
public class WorkOrderLineItemHelperTest {
    @testSetup static void setupTestData(){
        
        List<Account>accLst = new List<Account>();
        
        TestDataFactory.AccountParams AParms = new TestDataFactory.AccountParams();
        AParms.Name = 'Arco';
        AParms.ServicePartner = True;
        AParms.billingstreet = 'Santa Clara St';
        AParms.billingstate = '';
        
        List<Account> acclist1 = TestDataFactory.createTestAccount(1,AParms);
        accLst.addAll(acclist1);
        
        insert accLst;
        
        List<Case>csLst = new List<Case>();
        
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Subject = 'Payment not Received';
        cParms.Accountid = acclist1[0].Id;
        cParms.Status = 'New';
        cParms.Origin = 'Email';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        csLst.addAll(cslist1);
        
        insert csLst;
        
        List<WorkOrder>woLst = new List<WorkOrder>();
        
        TestDataFactory.WoParams woParms = new TestDataFactory.WoParams();
        woParms.Subject = 'Payment not Received';
        woParms.Accountid = acclist1[0].Id;
        woParms.Status = 'New';
        woParms.Caseid = cslist1[0].id;
        
        List<WorkOrder> wolist1 = TestDataFactory.createTestWorkOrder(1,woParms);
        woLst.addAll(wolist1);
        
        insert woLst;
        
        
        List<WorkOrderLineItem>woLiLst = new List<WorkOrderLineItem>();
        
        TestDataFactory.WoLiParams woLiParms = new TestDataFactory.WoLiParams();
        woLiParms.Location = 'CA';
        woLiParms.WorkOrderId = wolist1[0].Id;
        woLiParms.Status = 'New';
        
        List<WorkOrderLineItem> woLilst1 = TestDataFactory.createTestWorkOrderLines(3,woLiParms);
        woLiLst.addAll(woLilst1);
        
        insert woLiLst;
    }
    
    
    @isTest static void test_fecthExistingValues(){
        map<id,WorkOrderLineItem> woLimap = new map<id,WorkOrderLineItem> ([SELECT Id,WorkOrderId,Location__c,Status
                                                                            FROM WorkOrderLineItem]);
        Test.startTest();
        
        WorkOrderLineItemTriggerHandler wolihander = new WorkOrderLineItemTriggerHandler();
        wolihander.AfterInsert(woLimap);
        
        WorkOrderLineItemTriggerHandler wolihander1 = new WorkOrderLineItemTriggerHandler();
        wolihander1.AfterDelete(woLimap);
        WorkOrderLineItemTriggerHandler wolihander2 = new WorkOrderLineItemTriggerHandler();
        wolihander2.AfterUnDelete(woLimap);
        WorkOrderLineItemTriggerHandler wolihander3 = new WorkOrderLineItemTriggerHandler();
        wolihander3.AfterUpdate(woLimap, woLimap);
        Test.stopTest();
    }
    
    
}