/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Lead Trigger Test class


History
01-02-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
private class leadTriggerTest {
    
    @testSetup static void setupTestData(){
        List<Lead> leadlist = new  List<Lead>();
        
        TestDataFactory.LeadParams lParms = new TestDataFactory.LeadParams();
        lParms.FirstName = 'Test first name';
        lParms.LastName ='Test last name';
        lParms.Company = 'Test company'; 
        lParms.Status = 'Unqualified'; 
        lParms.LeadSource = 'Email';
        List<Lead> leadlist1 = TestDataFactory.createTestLeads(1,lParms);
        leadlist1[0].Assignusingactiveassignmentrule__c=TRUE;
        leadlist.addAll(leadlist1);
        TestDataFactory.LeadParams l2Parms = new TestDataFactory.LeadParams();
        l2Parms.FirstName = 'Test first name';
        l2Parms.LastName ='Test last name';
        l2Parms.Company = 'Test company'; 
        l2Parms.Status = 'Unqualified'; 
        l2Parms.LeadSource = 'Trade Show';
        List<Lead> leadlist2 = TestDataFactory.createTestLeads(1,l2Parms);
        leadlist2[0].Assignusingactiveassignmentrule__c=FALSE;
        leadlist.addAll(leadlist2);
        insert leadlist;
    }
    
    @isTest static void test_handleAfterInsert(){
        List <Lead> Leadlist= new List<Lead>([SELECT Id,Company,LeadSource,Status,Assignusingactiveassignmentrule__c
                                              FROM Lead]);
        Test.startTest();
        LeadTriggerHelper.handleLeadAssignment(Leadlist);
        Test.stopTest();
    }
    
    @isTest static void test_ErrorhandleAfterInsert(){
        
        Test.startTest();
        LeadTriggerHelper.handleLeadAssignment(null);
        Test.stopTest();
    }
    
}