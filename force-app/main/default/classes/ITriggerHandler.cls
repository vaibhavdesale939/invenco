/**************************************************************************************************************************************
* Apex Class: ITriggerHandler
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose: Trigger Interface
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
    1.0         Vaibhav D               FEB 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/

public interface  ITriggerHandler {
    
    
    // void BeforeInsert(List<SObject> newItems);
    
    //void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    
    //void BeforeDelete(Map<Id, SObject> oldItems);
    
    void AfterInsert(Map<Id, SObject> newItems);
    
    void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    
    void AfterDelete(Map<Id, SObject> oldItems);
    
    void AfterUndelete(Map<Id, SObject> newItems);   
    
}