/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    Lead Trigger Helper class for lead trigger

History
01-02-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
public class LeadTriggerHelper {
    
      public static void handleLeadAssignment(List<Lead> newLeadList){
      
      List<Lead> listLead = new List<Lead>();
      try{
        for (Lead thisLead : newLeadList) {
          if (thisLead.LeadSource == 'Email' && thisLead.Assignusingactiveassignmentrule__c== TRUE) {
                listLead.add(new Lead(id = thisLead.id));
            }
            else  if (thisLead.LeadSource != 'Phone' && thisLead.Assignusingactiveassignmentrule__c== FALSE){
                listLead.add(new Lead(id = thisLead.id));
            }
        }
      
		if(!listLead.isEmpty()){
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            Database.update(listLead, dmo);
        }
      }
      Catch(Exception ex){
            SaveResult result = new SaveResult().addError(ex.getMessage())
            .addErrorToLog(ex,null,null,'LeadTriggerHelper','handleLeadAssignment');
            result.getReturnValue();
        }
    }
}