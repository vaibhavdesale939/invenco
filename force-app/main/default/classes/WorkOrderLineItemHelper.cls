/**************************************************************************************************************************************
* Apex Class: WorkOrderLineItemHelper
* Created by Vaibhav D (Tranzevo)
---------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
1.To Update Locations from WO Line Item on Work Order. The Locations are used in  email templates sent to customer
---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
* History:
* - VERSION     DEVELOPER NAME          DATE                    DETAIL FEATURES
1.0         Vaibhav D               FEB 2020               INITIAL DEVELOPMENT
**************************************************************************************************************************************/


public class WorkOrderLineItemHelper {
    
    public static void updateLocationOnWorkOrder(list<WorkOrderLineItem> wolitemlst){
        
        Set<Id> woLineItemId = new Set<Id>(); 
        List<WorkOrder> workOrderlst= new List<WorkOrder>();
        
        try{
        for(WorkOrderLineItem woli:wolitemlst)
        {
            //get Work Order ID
            if(woli.Location__c != null){
                woLineItemId.add(woli.WorkOrderId);
            }
        }
        
        //get all locations from Work Order Line Items from the Ids in Set
        for(WorkOrder wo :[select Id, LineItemLocations__c ,(select id,Location__c from WorkOrderLineItems) from WorkOrder where Id =:woLineItemId])
        {
            String lineItems = '';
            for(WorkOrderLineItem woli : wo.WorkOrderLineItems){
                //Add Location
                if(lineItems == NULL || lineItems == ''){
                    lineItems = woli.Location__c + ',';
                }
                //concate location
                else{
                    lineItems = lineItems + woli.Location__c + ',';
                }
                
            }
            wo.LineItemLocations__c = lineItems.removeEnd(',');
            workOrderlst.add(wo);
        }
        
        //update work order
        if(!workOrderlst.IsEmpty())
        {
            update workOrderlst;
        }
        }
        
        catch (exception e){
            SaveResult result = new SaveResult().addError(e.getMessage())
                .addErrorToLog(e,null,null,'WorkOrderLineItemHelper','updateLocationOnWorkOrder');
            result.getReturnValue();
        }
    }
    
}