/*----------------------------------------------------------------------------------------------
Author:         Ruchita Trivedi
Company:        Tranzevo
Description:    SupportAuraController Apex class for Support_Form component


History
20-03-2020      Ruchita Trivedi        Initial Release
----------------------------------------------------------------------------------------------*/
public class SupportAuraController {
     
    @AuraEnabled
    public static List <String> getPiklistValues() {
        List<String> plValues = new List<String>();
         
        //Get the object type from object name. 
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Case');
         
        //Describe the sObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
        //Get the specific field information from field name.
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Brand__c').getDescribe();
         
        //Get the picklist field values.
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
         
        //Add the picklist values to list.
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        //plValues.sort();
        return plValues;
    }
    
   @AuraEnabled
    public static List <String> getIssuePiklistValues() {
        List<String> plValues = new List<String>();
         
        //Get the object type from object name. 
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Case');
         
        //Describe the sObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
        //Get the specific field information from field name. 
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('IssueTypeReported__c').getDescribe();
         
        //Get the picklist field values.
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
         
        //Add the picklist values to list.
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        plValues.sort();
        return plValues;
    }
    
}